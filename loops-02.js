//**Iteración #2: Condicionales avanzados**

// Comprueba en cada uno de los usuarios que tenga
// al menos dos trimestres aprobados y añade la propiedad ***isApproved*** a true o false
// en consecuencia. Una vez lo tengas compruébalo con un ***console.log***.

const alumns = [
    {name: 'Pepe Viruela', T1: false, T2: false, T3: true}, 
	{name: 'Lucia Aranda', T1: true, T2: false, T3: true},
	{name: 'Juan Miranda', T1: false, T2: true, T3: true},
	{name: 'Alfredo Blanco', T1: false, T2: false, T3: false},
	{name: 'Raquel Benito', T1: true, T2: true, T3: true}
]
// let aprobados = [];
// for (let i = 0; i < alumns.length; i++) {
//     const alumn = alumns[i];
//     //console.log(alumn);
//     let trimestreAprove = 0;
//     for ( propiedad in alumn){
        
//         if (alumn[propiedad] == true) {
//             trimestreAprove++;
//         }

//         if (trimestreAprove>=2){
//             aprobados.push(alumn);
//             Object.defineProperty(alumn,'isApproved',{ value: true,});
//         }
//     }
    
    
// }

alumns.forEach(alumno => {
    ((alumno.T1 + alumno.T2 + alumno.T3) >= 2) ? alumno.isApproved=true : alumno.isApproved=false;
});
//console.log(aprobados);
console.log(alumns);