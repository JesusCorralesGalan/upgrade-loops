//Iteración #5: ProbandoFor
// Usa un bucle for para recorrer todos los destinos del array y elimina 
// los elementos que tengan el id 11 y 40. Imprime en un console log el array. 

const placesToTravel2 = [{id: 5, name: 'Japan'}, 
                        {id: 11, name: 'Venecia'},
                        {id: 23, name: 'Murcia'},
                        {id: 40, name: 'Santander'},
                        {id: 44, name: 'Filipinas'},
                        {id: 59, name: 'Madagascar'}
                        ]

for (let i = 0; i < placesToTravel2.length; i++) {
    const element = placesToTravel2[i];
    if (element.id ==11 ){
    delete placesToTravel2[i];
    
    
    }
    else if (element.id == 40){
     delete placesToTravel2[i];
   
    }
    
    
}


console.log(placesToTravel2);